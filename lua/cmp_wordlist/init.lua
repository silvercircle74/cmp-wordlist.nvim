-- simple cmp source for completing items from a word list.
-- based on sample code in the documentation of the CMP plugin at:
-- https://github.com/hrsh7th/nvim-cmp

-- requirements:
-- * plenary
-- * nvim-cmp
--
-- LICENSE: MIT

--- a word list is a simple text file containing one word by line and optionally a translation term, separated
--  by the separation character. By default, the pipe symbol (|) is used for separation.
--  You can use it to complete long words or common abbreviations.
--  examples:
--
--  mycoolword
--  afk|away from keyboard
--  nvim|Neovim
--  averylongwordidonotwanttotypealways
--
--  and so on.
--  When a translation term is present, it will be used for completion. Otherwise, the word itself will be
--  inserted. A translation term will be shown as documentation float when CMP is active.
--
--  the word files are lazily read when completion is first invoked. It is possible to manually refresh them
--  by calling: require("cmp_wordlist").rebuild_list()

local _status, telescope_themes = pcall(require, "telescope.themes")

local defaults = {
  -- filenames from which the word list is built. They can be absolute filenames or are 
  -- treated a relative to stdpath("config")
  -- all these default values can be changed via setup()
  auto_list_name = "cmp_wordlist_auto.txt",
  wordfiles = { },
  enabled = true,         -- this has currently no effect
  debug = false,
  debug_lists = false,
  read_on_setup = false,
  watch_files = false,    -- use libuv file watchers to re-scan changed wordfiles
  separator = "|",        -- the character used to separate the word from its translation
  telescope_theme = _status and telescope_themes.get_dropdown or nil,
  blink_compat = false,
  blink_do_undo = false,
}

local source = {}
local conf = {}

local wordlist = {}
local wordindex = {}

local wordfiles = {}
local initial_list_built = false

local _utils = require("cmp_wordlist.utils")

-- write the "auto" wordfile to disk
local function write_auto_list()
  local path = require("plenary.path")
  local p = path:new(conf.auto_word_list)
  local final_filename

  if p:is_absolute() then
    final_filename = p:expand()
  else
    local lpath = path:new(vim.fn.stdpath("config"), defaults.auto_list_name)
    final_filename = lpath:expand()
  end
  local file = io.open(final_filename, "w+")
  if file ~= nil then
    for k,v in pairs(wordlist['l_auto']) do
      if v.translation ~= nil then
        file:write(k .. conf.separator .. v.translation .. "\n")
      else
        file:write(k .. "\n")
     end
    end
    file:flush()
    io.close(file)
  end
end

-- rebuild the wordindex from the word lists
local function rebuild_index()
  wordindex = {}
  collectgarbage()
  for k,_ in pairs(wordlist) do
    for i,_ in pairs(wordlist[k]) do
      table.insert(wordindex, { label = i, list = k })
    end
  end
  if conf.debug_lists == true then
    print(vim.inspect(wordindex))
  end
end

--- add the contens of a file to a word list
--- @param file string: the fully qualified file name
--- @param index string: the list key
local function add_to_list(file, index)
  if vim.fn.filereadable(file) then
    source.debugmsg("Add words from: " .. file .. " to list " .. index)
    local f = io.open(file)
    if f ~= nil then
      local lines = f:lines()
      local lindex = "l_" .. index
      wordlist[lindex] = {}
      for line in lines do
        if #line > 0 then
          if string.find(line, "|") ~= nil then
            local elems = _utils.string_split(line, conf.separator)
            wordlist[lindex][elems[1]] = { translation = elems[2], index = index }
          else
            wordlist[lindex][line] = { index = index  }
          end
        end
      end
      io.close(f)
    end
  end
end

function source.debugmsg(msg)
  if conf.debug == true then
    print(msg)
  end
end

function source.new()
  return setmetatable({}, { __index = source })
end

---Return whether this source is available in the current context or not (optional).
---@return boolean
function source:is_available()
  return true
end

---Return the debug name of this source (optional).
---@return string
function source:get_debug_name()
  return 'wordlist'
end

---Return the keyword pattern for triggering completion (optional).
---If this is ommited, nvim-cmp will use a default keyword pattern. See |cmp-config.completion.keyword_pattern|.
---@return string
function source:get_keyword_pattern()
  return [[\k\+]]
end

---Return trigger characters for triggering completion (optional).
function source:get_trigger_characters()
-- return { ':' }
  return { '@' }
end

function source:complete(params, callback)
-- rebuild list on first completion attempt. don't jeopardize worthy startup time 😺
  if initial_list_built == false then
    source:rebuild_list()
    initial_list_built = true
  end
  callback(wordindex)
end

--- Resolve completion item (optional). This is called right before the completion is about to be displayed.
--- Useful for setting the text shown in the documentation window (`completion_item.documentation`).
function source:resolve(completion_item, callback)
  if completion_item.word == nil and wordlist[completion_item.list][completion_item.label].translation ~= nil then
    completion_item.word = wordlist[completion_item.list][completion_item.label].translation
    completion_item.detail = "Translates to: " .. wordlist[completion_item.list][completion_item.label].translation
  end
  callback(completion_item)
 end

---Executed after the item was selected.
function source:execute(completion_item, callback)
  if conf.blink_compat == true and completion_item.word ~= nil then
    local row, col = unpack(vim.api.nvim_win_get_cursor(0))
    local line = vim.api.nvim_get_current_line()
    col = completion_item.cursor_column
    local nline = line:sub(0, completion_item.textEdit.range.start.character)
      .. completion_item.word .. line:sub(completion_item.textEdit.range["start"].character + vim.fn.strchars(completion_item.textEdit.newText) + 1)
    if conf.blink_do_undo == true then
      vim.api.nvim_set_option('undolevels', vim.api.nvim_get_option('undolevels'))
    end
    vim.api.nvim_set_current_line(nline)
    vim.api.nvim_win_set_cursor(0, { row, col + #completion_item.word - 1 } )
  end
  callback(completion_item)
end

function source.setup(options)
  conf = vim.tbl_deep_extend('force', defaults, options)
  table.insert(conf.wordfiles, defaults.auto_list_name )
  if conf.read_on_setup == true then
    -- build all normal word files
    source:rebuild_list()
    initial_list_built = true
  end
end

local function onChange(cust, _, _, status)
  if not status.change then
    return
  end
  for _,v in pairs(wordfiles) do
    if v.file == cust then
      v.changed = true
      source.debugmsg("rebuilding for index " .. v.index .. " (" .. v.file .. ")")
      source:rebuild_list()
      vim.loop.fs_event_stop(v.watch)
      vim.loop.fs_event_start(v.watch, v.file, { }, vim.schedule_wrap(function(...) onChange(v.file, ...) end))
    end
  end
end

--- rebuild the wordlist
--- rebuilds only the wordfiles that have their changed field set to true
function source:rebuild_list()
  local index = 1

  source.debugmsg("List rebuild, start")
  local path = require("plenary.path")
  -- first run, build word files, create file watches and set them all to changed
  if #wordfiles == 0 then
    for _,v in pairs(conf.wordfiles) do
      local p = path:new(v)
      if p:is_absolute() and p:is_file() then
        if vim.fn.filereadable(p:expand()) then
          local watch = conf.watch_files == true and vim.loop.new_fs_event() or nil
          local is_auto_list = p:expand() == defaults.auto_list_name and true or false
          if conf.watch_files == true and watch ~= nil and is_auto_list == false then
            watch:start(p:expand(), {}, vim.schedule_wrap(function(...) onChange(p:expand(), ...) end))
            source.debugmsg("Starting file watcher for: ", p:expand())
          end
          table.insert(wordfiles, { file=p:expand(), watch=watch, changed = true, index = index, autolist = is_auto_list })
          index = index +1
        end
      else
        local final_path = path:new(vim.fn.stdpath("config"), p:expand())
        if final_path:is_file() then
          local watch = conf.watch_files == true and vim.loop.new_fs_event() or nil
          local is_auto_list = p:expand() == defaults.auto_list_name and true or false
          if conf.watch_files == true and watch ~= nil and is_auto_list == false then
            vim.loop.fs_event_start(watch, final_path:expand(), {}, vim.schedule_wrap(function(...) onChange(final_path:expand(), ...) end))
            source.debugmsg("Starting file watcher for: " .. final_path:expand())
          end
          table.insert(wordfiles, { file=final_path:expand(), watch=watch, changed = true, index = index, autolist = is_auto_list} )
          index = index + 1
        end
      end
    end
  end
  if #wordfiles == 0 then
    vim.notify("cmp_wordlist: Warning, no word files defined or found", 3)
    return
  end
  if conf.debug_lists == true then
    print(vim.inspect(wordfiles))
  end
  -- iterate over word files and add changed files to our word list
  for _,v in pairs(wordfiles) do
    if v.changed == true then
      if v.autolist == true then
        add_to_list(v.file, 'auto')
      else
        add_to_list(v.file, v.index)
      end
      v.changed = false
    end
  end
  if conf.debug_lists == true then
    print(vim.inspect(wordlist))
  end
  rebuild_index()
end

--- add the word to the auto_word_list. It just adds it to wordlist['l_auto'] and rebuilds
--- the word index.
--- @param word string: the word to add
--- @param translation string|nil : the translated word (optional, can be nil or "")
local function raw_add_word(word, translation)
  if wordlist['l_auto'][word] ~= nil then
    vim.notify("The word " .. word .. " is already on the list. Duplicates are not allowed.")
    return
  end
  if translation ~= nil and #translation > 0 then
    wordlist['l_auto'][word] = { index = 'auto', translation = translation }
  else
    wordlist['l_auto'][word] = { index = 'auto' }
  end
  rebuild_index()
  if conf.debug_lists == true then
    print(vim.inspect(wordlist))
  end
  write_auto_list()
end

--- add the word without translation to the auto list
--- @param word string: the word to add
function source.add_word(word)
  raw_add_word(word, nil)
end

--- add the word with a translation term to the auto list
--- @param word string: The word to add
--- @param translation string: The term to autocomplete (translation)
function source.add_translated_word(word, translation)
  raw_add_word(word, translation)
end

--- add the word under the cursor to the auto list.
function source.add_cword()
  raw_add_word(vim.fn.expand("<cword>"), nil)
end

function source.add_cword_with_translation()
  local translation = vim.fn.input("Translation for " .. vim.fn.expand("<cword>") .. ": ")
  if translation ~= nil and #translation > 0 then
    raw_add_word(vim.fn.expand("<cword>"), translation)
  end
end

--- this implements a telescope picker showing the words in the auto list
--- <C-d> is mapped to delete word
--- TODO: allow to enter the entry (add/edit translation)
function source.autolist()
  local max_width = 80

  if pcall(require, 'telescope') == false then
    source.debugnotify("This feature requires the Telecope plugin.")
    return
  end
  local pickers = require "telescope.pickers"
  local finders = require "telescope.finders"
  local tconf = require("telescope.config").values
  local actions = require "telescope.actions"
  local action_state = require "telescope.actions.state"
 --  local make_entry = require "telescope.make_entry"
  if #wordlist == 0 and initial_list_built == false then
    source:rebuild_list()
  end
  -- use telescope
  local selector = function(opts)
    opts = opts or {}
    pickers.new(opts, {
      prompt_title = "Browse custom wordlist",
      layout_config = {
        horizontal = {
          prompt_position = "bottom"
        }
      },
      finder = finders.new_table {
        results = wordindex,
        entry_maker = function(entry)
          if entry.list ~= 'l_auto' then
            return nil
          end
          local translation = wordlist['l_auto'][entry.label].translation ~= nil and wordlist['l_auto'][entry.label].translation or nil
          return {
            value = entry,
            display = function()
              if translation ~= nil then
                return _utils.truncate(_utils.rpad(entry.label, 30, ' ') .. _utils.rpad(translation, 50, ' '), max_width)
              else
                return _utils.truncate(entry.label, max_width)
              end
            end,
            ordinal = translation ~= nil and (entry.label .. translation) or entry.label
          }
        end
      },
      sorter = tconf.generic_sorter(opts),
      attach_mappings = function(prompt_bufnr, map)
        map('i', '<c-d>', function(_)
          local picker = action_state.get_current_picker(prompt_bufnr)
          picker:delete_selection(function(selection)
            source.debugmsg("Remove word " .. selection.value.label .. " from the auto list")
            wordlist['l_auto'][selection.value.label] = nil
            rebuild_index()
            write_auto_list()
          end)
        end)
        map('i', '<c-q>', function(_) end)    -- remap this to nothing, it will otherwise produce an error
        actions.select_default:replace(function()
          -- local path = require("plenary.path")
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          print(selection.value.label)
          rebuild_index()
        end)
      return true
      end,
    }):find()
  end
  selector(conf.telescope_theme({layout_config={width=80, height=0.5}, prompt_title="Auto Word list, <C-d>deletes word"}))
end

-- Register your source to nvim-cmp.
require('cmp').register_source('wordlist', source)

return source

