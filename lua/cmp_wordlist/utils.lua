-- local utils
-- can be used without calling setup(), but you can use it to set some (few) options.

local M = {}

--- split a string at delimiter
--- @param s string: string to split
--- @param delimiter string: where to split
--- @return table: at least 2 substrings
function M.string_split(s, delimiter)
  local result = {};
  for match in (s..delimiter):gmatch("(.-)"..delimiter) do
    table.insert(result, match);
  end
  return result;
end

function M.rpad(string, length, fill)
  local padlen = (length - #string)
  if #string >= length or padlen < 2 then
    return string
  end
  return string .. string.rep(fill, padlen)
end

function M.truncate(text, max_width)
  if #text > max_width then
    return string.sub(text, 1, max_width) .. "…"
  else
    return text
  end
end

function M.invalidate_autolist(wordfiles)
  for _,v in pairs(wordfiles) do
    if v.autolist == true then
      v.changed = true
    end
  end
end

function M.pad(string, length, fill)
  local padlen = (length - #string) / 2
  if #string >= length or padlen < 2 then
    return string
  end
  return string.rep(fill, padlen) .. string .. string.rep(fill, padlen)
end

function M.lpad(string, length, fill)
  local padlen = (length - #string)
  if #string >= length or padlen < 2 then
    return string
  end
  return string.rep(fill, padlen) .. string
end

return M

