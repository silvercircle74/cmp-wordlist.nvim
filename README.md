# Cmp Wordlist

This is a custom completion source for the [cmp completion engine](https://github.com/hrsh7th/nvim-cmp) 
for the Neovim text editor.

## Its requirements are:

* [cmp](https://github.com/hrsh7th/nvim-cmp)
* [The plenary plugin](https://github.com/nvim-lua/plenary.nvim)
* [The Neovim Editor](https://github.com/neovim/neovim) (obviously)

## Installation

Install it with any plugin manager, e.g. packer:

```
use 'https://gitlab.com/silvercircle74/cmp-wordlist.nvim'
```

## Concept

The plugin can manage an arbitrary number of `Word Files`. Each word file contains a single word per 
line. An optional translation term, separated from the worth by a specific character (| by default) can 
be used. The contents of all word files generate a completion source for the CMP completion engine. CMP 
allows you to „auto-complete“ the words.

## Translation terms

Any entry in a word file can contain a translation term, for example:
```
afk|Away from Keyboard
```
When you auto-complete the term `afk`, it will actually insert the text `Away from Keyboard`. 

## Handling of duplicate entries

A single word file is not allowed to contain duplicates. However, it is possible to have the same term in 
different word files, using different translations. The auto-complete popup will then suggest multiple 
choices for a word

## Editing word files while Neovim is running

This is supported. The plugin uses file watchers to monitor the word files for changes. It will rescan a 
file, when it's modified and rebuild the internal list of words.

## Setup

Here are the default options. By default, no word files are used, so the `wordfiles =` entry is the minimum 
configuration you have to provide. It is a list of filenames which can be either absolute (including a 
full path qualifier) or relative (a file name alone). When relative, they must be relative to the Neovim 
configuration directory. This is the directory that holds your `init.vim` or `init.lua`.

In the example below, `wordlist.txt` is expected to be in `$HOME/.config/nvim` (or the equivalent folder 
on Windows or macOS).
```lua
require("cmp_wordlist").setup({
  auto_list_name = "cmp_wordlist_auto.txt",
  wordfiles = { 'wordlist.txt' },
  enabled = true,         -- this has currently no effect
  debug = false,
  debug_lists = false,
  read_on_setup = false,
  watch_files = false,    -- use libuv file watchers to re-scan changed wordfiles
  separator = "|",        -- the character used to separate the word from its translation
  telescope_theme = telescope_theme,
})
```

